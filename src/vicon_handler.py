#!/usr/bin/env python2

"""
ROS node to handle VICON data as through the VRPN server running as part of 
the VICON Tracker software.

Reads topics as exposed by the ROS node vrpn_client_ros.

Author: Ian Loefgren
Date created: 2/5/2018

Inputs:
    Vicon data as following topics:
    - /vrpn_client_node/$(object_name)/pose [type: geometry_msgs::PoseStamped] 
    - /vrpn_client_node/$(object_name)/twist [type: geometry_msgs::TwistStamped]
    - /vrpn_client_node/$(object_name)/accel [type: geometry_msgs::TwistStamped]
Outputs:

"""

import sys
import rospy
import tf
import math
import time
from geometry_msgs.msg import PoseStamped, TwistStamped

class VICONHandler(object):

    def __init__(self,self_name=None,target_name=None):
        self.self_model_name = self_name
        self.target_model_name = target_name

        self.self_pose = None
        self.target_pose = None

        self.self_flag = False
        self.target_flag = False

        if self.self_model_name == "tars":
            self.rel_pub_flag = True
        else:
            self.rel_pub_flag = False

        rospy.init_node('{}_vicon_handle'.format(self.self_model_name))

        rospy.Subscriber('/vrpn_client_node/{}/pose'.format(self.self_model_name),PoseStamped,self.self_pose_cb)
        # rospy.Subscriber('/vrpn_client_node/{}/twist'.format(self.model_name),TwistStamped,self.vel_cb)
        # rospy.Subscriber('/vrpn_client_node/{}/accel'.format(self.model_name),TwistStamped,self.accel_cb)

        rospy.Subscriber('/vrpn_client_node/{}/pose'.format(self.target_model_name),PoseStamped,self.target_pose_cb)

        if self.rel_pub_flag:
            self.rel_pub = rospy.Publisher('/relative_position',PoseStamped,queue_size=100)

        # rospy.Rate(1)

        rospy.spin()

    def self_pose_cb(self,msg):
        x, y, z = [msg.pose.position.x, msg.pose.position.y, msg.pose.position.z]
        roll, pitch, yaw = tf.transformations.euler_from_quaternion(
            (msg.pose.orientation.x,msg.pose.orientation.y,msg.pose.orientation.z,msg.pose.orientation.w))
        self.self_pose = [x,y,z,roll,pitch,yaw]
        self.self_flag = True

        if self.self_flag and self.target_flag and self.rel_pub_flag:
            rel_pose = [0,0,0,0,0,0]
            rel_pose[0] = -self.self_pose[0] + self.target_pose[0]
            rel_pose[1] = -self.self_pose[1] + self.target_pose[1]
            rel_pose[2] = -self.self_pose[2] + self.target_pose[2]
            rel_pose[3] = -self.self_pose[3] + self.target_pose[3]
            rel_pose[4] = -self.self_pose[4] + self.target_pose[4]
            rel_pose[5] = -self.self_pose[5] + self.target_pose[5]
            quat = tf.transformations.quaternion_from_euler(rel_pose[3],rel_pose[4],rel_pose[5])
            pub_msg = PoseStamped()
            pub_msg.header.stamp = rospy.Time.now()
            pub_msg.pose.position.x = rel_pose[0]
            pub_msg.pose.position.y = rel_pose[1]
            pub_msg.pose.position.z = rel_pose[2]
            pub_msg.pose.orientation.x = quat[0]
            pub_msg.pose.orientation.y = quat[1]
            pub_msg.pose.orientation.z = quat[2]
            pub_msg.pose.orientation.w = quat[3]

            self.rel_pub.publish(pub_msg)

            self.self_flag = False
            self.target_flag = False
        
    def target_pose_cb(self,msg):
        x, y, z = [msg.pose.position.x, msg.pose.position.y, msg.pose.position.z]
        roll, pitch, yaw = tf.transformations.euler_from_quaternion(
            (msg.pose.orientation.x,msg.pose.orientation.y,msg.pose.orientation.z,msg.pose.orientation.w))
        self.target_pose = [x,y,z,roll,pitch,yaw]
        self.target_flag = True
        

    def vel_cb(self,msg):
        pass

    def accel_cb(self,msg):
        pass

if __name__ == "__main__":
    # self_name = rospy.get_param('self_model_name')
    # target_name = rospy.get_param('target_model_name')
    self_name = sys.argv[1]
    target_name = sys.argv[2]
    handler = VICONHandler(self_name=self_name,target_name=target_name)





