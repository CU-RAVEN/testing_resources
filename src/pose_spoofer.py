#!/usr/bin/env python2

import rospy
import sys
import time
from geometry_msgs.msg import PoseStamped, TwistStamped

def spoof():
    rospy.init_node('gimbal_pos_spoofer')
    pub = rospy.Publisher('/vrpn_client_node/{}/pose'.format(model_name),PoseStamped,queue_size=100)
    rate = rospy.Rate(int(rate_val))
    while not rospy.is_shutdown():
        msg = PoseStamped()
        msg.pose.position.x = 0
        msg.pose.position.y = 0
        msg.pose.position.z = 0.5
        msg.pose.orientation.x = 0
        msg.pose.orientation.y = 0
        msg.pose.orientation.z = 0
        msg.pose.orientation.w = 1
        pub.publish(msg)
        rate.sleep()

if __name__ == "__main__":
    model_name = sys.argv[1]
    rate_val = sys.argv[2]
    spoof()