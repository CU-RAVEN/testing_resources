# RAVEN Testing Resources

### General testing resources for CU Boulder 2017-2018 Senior Design team Rover and Air Visual Environment Navigation

#### Install and Run Instructions
To install, clone the respository into the src/ directory of a catkin workspace and then run _catkin_make_ from the top level of that workspace.   
To run, in a terminal where you have sourced your ROS environment and the aforementioned catkin workspace, simply run the command 
```
$ roslaunch testing_resources vicon_test.launch
```

__Note:__ If you have trouble running the handler nodes because the python file cannot be found, navigate to the src/ directory and run the command
```
$ chmod +x vicon_handler.py
```
to make the python file excecutable.

#### __vicon_test.launch__
ROS launch file to launch VICON handler nodes for both the UAV and UGV, as well as a _vrpn_client_ros_ node to interface with VICON motion capture system using the virtual reality peripheral network (VRPN), as well as a rosbag recording of all topics.

- Arg "server" uses the specified IP address to find the vrpn server
- Param "update_frequency" specifies the rate at which new VICON data should be received (default 100 Hz)

#### __vicon_handler.py__
Node that subscribes to VICON datastream for UAV and UGV and finds the relative position between the two. This relative position is then published. The handler waits until the positions of both 



